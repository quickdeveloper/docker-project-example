from django.test import TestCase
from users.models import User

# Create your tests here.

class UserCase(TestCase):
    def setUp(self):
        User.objects.create(first_name="Pedro", last_name="Perez", age=33)
        User.objects.create(first_name="Maria", last_name="Perez", age=33)

    def test_create_user(self):
        user = User.objects.get(first_name="Maria")
        self.assertEqual(user.first_name, 'Maria')

    def test_quantity_user(self):
        user = User.objects.filter(age=33)
        self.assertEqual(user.count(), 2)